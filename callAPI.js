const fetch = require("node-fetch");
const apiURL = "https://blockchain.info/ticker";

module.exports.callAPI = function() {
    return fetch(apiURL)
    .then(checkStatus)
    .then(respond => respond.json())
    .catch(console.error);
}

function checkStatus(status) {
    if (status.ok) {
        return status;
    }
    else {
        throw new Error("Request cannot be process. Status is not 200");
    }
}
