const http = require("http");
const myModule = require("./firstModule.js");
const api = require("./callAPI.js");

http.createServer(function(req, res) {
  api.callAPI().then(x => {
    let respond = x;
    console.log(respond);
    res.writeHead(200, {'Content-Type':'application/json'});
    res.write(JSON.stringify(respond));
    res.end();
  });
}).listen(8080);
