module.exports.myDate = function() {
  return "Today is: " + Date();
}

module.exports.myTime = function() {
  var date = new Date();
  var hour = date.getHours();
  return "Current time is: " + hour;
}
